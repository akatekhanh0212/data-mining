# Data-Mining

This is the project for assignment

# Topic

Imbalanced classification using sampling

# Data set

https://www.kaggle.com/mlg-ulb/creditcardfraud

# Step

1. Model: k-Nearest Neighbor, Neural Nets, etc (key idea, mô tả kỹ thuật, đưa ra
   Pros/Cons, tính phổ dụng).
2. Tìm hiểu một số kỹ thuật về sampling (upper/under sampling, ví dụ như SMOTE).
3. Một số ứng dụng trong việc phát hiện bất thường.
4. Hiện thực.
5. Thực nghiệm (Datasets: https://www.kaggle.com/mlg-ulb/creditcardfraud, https://arxiv.org/pdf/1106.1813.pdf, …)
6. Phân tích và đánh gia (accuracy, confusion matrix)
